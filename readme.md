Connect X
=========

Term project of Computational Intelligence course at FIT, CTU, Prague.

[Connect X](https://www.kaggle.com/competitions/connectx) is a Kaggle Competition of Reinforcement Learning.

Scripts to generate training data are in `/code` directory.

Model training using precomputed data is accessible as [Colaboratory notebook](https://colab.research.google.com/drive/1yXqIkASKwPDdkbVBL-SXA2kop3NMlKWA).

Training of the Deep Value Network (DQN) is at [Kaggle](https://www.kaggle.com/code/jaroslavlanger/connect-x-dqn).

For more information see `report.pdf` in the root of this repository.

How to Submit Multiple Files
----------------------------

*.tar.gz*

Kaggle says, you can submit multiple files "in a zip/gz/7z archive".
`.tar.gz` works for me, `.zip` doesn't.
I found it in a [discussion about Kaggle's Lux AI competition](https://www.kaggle.com/competitions/lux-ai-2021/discussion/282631#1565006).
I thought it will not be relevant for Connect X, but apparently it is.

When submitting `.zip` I got this error:

```py
Traceback (most recent call last):
  File "/opt/conda/lib/python3.7/site-packages/kaggle_environments/agent.py", line 43, in get_last_callable
    code_object = compile(raw, path, "exec")
  File "/kaggle_simulations/agent/main.py", line 1
    /kaggle_simulations/agent/main.py
    ^
SyntaxError: invalid syntax

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/opt/conda/lib/python3.7/site-packages/kaggle_environments/agent.py", line 159, in act
    action = self.agent(*args)
  File "/opt/conda/lib/python3.7/site-packages/kaggle_environments/agent.py", line 125, in callable_agent
    agent = get_last_callable(raw_agent, path=raw) or raw_agent
  File "/opt/conda/lib/python3.7/site-packages/kaggle_environments/agent.py", line 64, in get_last_callable
    raise InvalidArgument("Invalid raw Python: " + repr(e))
kaggle_environments.errors.InvalidArgument: Invalid raw Python: SyntaxError('invalid syntax', ('/kaggle_simulations/agent/main.py', 1, 1, '/kagg
```

*Python 3.7*

You can see in the log above, the complaining python's version is `3.7`.
This of course leads to some usage restrictions such as no type-hints.

*/kaggle_simulations/agent*

Agent's current working directory is `/kaggle/working`.
However the files extracted from your submission archive ends up in `/kaggle_simulations/agent` directory.
I found about it in this [comment](https://www.kaggle.com/product-feedback/215421#1181365).

*Debugging*

A great, great [comment](https://www.kaggle.com/c/halite/discussion/177686#1215686) was made about using Exceptions in order to debug submissions.
You can submit the code for debugging purposes without wasting a submission count.
The solution is to print all the stuff you are interested to debug.
Then after some moves raise an Exception. E.g.

```py
def agent(obs, config):
    ...
    if obs.step > 10:
        raise Exception('No wasted submission because of debugging.')
```

References
----------

* [Getting Started Notebook](https://www.kaggle.com/code/ajeffries/connectx-getting-started/notebook)

* [Connect X (kaggle.com)](https://www.kaggle.com/competitions/connectx/overview/description)
* [Negamax (wikipedia)](https://en.wikipedia.org/wiki/Negamax)
* [Reinforcement Learning course by Deep Mind](https://www.deepmind.com/learning-resources/reinforcement-learning-lecture-series-2021)
* [Reinforcement Learning: An Introduction by Richard S. Sutton and Andrew G. Barto](http://incompleteideas.net/book/the-book.html)
* [Mastering Atari, Go, Chess and Shogi by Planning with a Learned Model](https://arxiv.org/abs/1911.08265)
* [Tic-tac-toe with CNN](https://medium.com/@carsten.friedrich/part-7-this-is-deep-in-a-convoluted-way-f56124cdd64b)
    * [tic-tac-toe github](https://github.com/fcarsten/tic-tac-toe)

* [MiniMax](https://en.wikipedia.org/wiki/Minimax)
* [Monte Carlo Tree Search](https://en.wikipedia.org/wiki/Monte_Carlo_tree_search)

* [Pytorch DQN](https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html)

* [Stable baseline](https://stable-baselines3.readthedocs.io/en/master/)

* [MuZero](https://www.deepmind.com/blog/muzero-mastering-go-chess-shogi-and-atari-without-rules)
    * [MuZero paper](https://www.nature.com/articles/s41586-020-03051-4.epdf?sharing_token=kTk-xTZpQOF8Ym8nTQK6EdRgN0jAjWel9jnR3ZoTv0PMSWGj38iNIyNOw_ooNp2BvzZ4nIcedo7GEXD7UmLqb0M_V_fop31mMY9VBBLNmGbm0K9jETKkZnJ9SgJ8Rwhp3ySvLuTcUr888puIYbngQ0fiMf45ZGDAQ7fUI66-u7Y%3D)
* [MuZero tic-tac-toe implementation](https://github.com/werner-duvaud/muzero-general/blob/master/games/tictactoe.py)
* [AlphaGo](https://www.deepmind.com/research/highlighted-research/alphago)
* [AlphaGo Zero](https://www.deepmind.com/blog/alphago-zero-starting-from-scratch)

* [Kaggle AlphaZero baseline 1400](https://www.kaggle.com/code/connect4alphazero/alphazero-baseline-connectx)
* [Kaggle Pytorch DQN 905 score](https://www.kaggle.com/code/matant/pytorch-dqn-connectx)
* [Kaggle Keras DQN 555](https://www.kaggle.com/code/phunghieu/connectx-with-deep-q-learning)
* [Kaggle DQN pytorch](https://www.kaggle.com/code/phunghieu/connectx-with-deep-q-learning-pytorch)
* [Kaggle DQN weights only 435](https://www.kaggle.com/code/jtbontinck/deep-q-network-w-o-package-inference-only)
* [Kaggle  Pytorch DQN???](https://www.kaggle.com/code/nxhong93/deep-q-learning)
* [Kaggle stable baseline](https://www.kaggle.com/code/alexisbcook/deep-reinforcement-learning)
