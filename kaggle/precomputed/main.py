import numpy as np
import torch
from torch import nn
import torch.nn.functional as F

device = 'cpu'
ROOT = '/kaggle_simulations/agent'
WEIGHTS_PATH = '{}/preferences_6_7_4_3e15.pth'.format(ROOT)

def encode_state_one_hot(state, *, row_cnt, col_cnt):
    (board_str, mark) = state
    arr_2d = np.array([int(s) for s in board_str]).reshape((row_cnt, col_cnt))

    arr_3d = np.array([(arr_2d == dim).astype(int) for dim in range(3)])
    arr_flat = arr_3d.flatten()

    marks = np.zeros(2, dtype=int)
    marks[mark-1] = 1
    return np.concatenate((arr_flat, marks))

class TransitionConvNet(nn.Module):
    """Convolutional Neural Network configured for modeling Transitions."""

    def __init__(self, *, feat_len, targ_len, hidden_cnt, hidden_neurons, conv_shape):
        super(TransitionConvNet, self).__init__()
        self.shape = conv_shape
        self.conv_size = np.prod(self.shape)
        out_channels = 3
        kernel_size = 4
        padding = (kernel_size-1, kernel_size-1)
        self.convolutional = nn.Conv2d(in_channels=self.shape[0],
                                       out_channels=out_channels,
                                       kernel_size=kernel_size, padding=padding)
        in_neurons = (out_channels * ((self.shape[-2] + 2*padding[-2] - kernel_size + 1)*
                                      (self.shape[-1] + 2*padding[-1] - kernel_size + 1))
                      + (feat_len - self.conv_size))
        self.sequential = nn.Sequential(
            nn.Linear(in_neurons, hidden_neurons),
            nn.ReLU(),
            *(fn for hidden_idx in range(hidden_cnt) for fn in (
                nn.Linear(hidden_neurons, hidden_neurons),
                nn.ReLU())),
            nn.Linear(hidden_neurons, targ_len),
        )

    def forward(self, x):
        return self.sequential(
            torch.cat(
                (torch.flatten(self.convolutional(x[:, :self.conv_size].view(-1, *self.shape)),
                               start_dim=1),
                 x[:, self.conv_size:]),
                dim=1),
        )

model = TransitionConvNet(feat_len=128,
                          targ_len=7,
                          hidden_cnt=1,
                          hidden_neurons=64,
                          conv_shape=(3,6,7),
                          )
model.load_state_dict(
    torch.load(WEIGHTS_PATH, map_location=torch.device(device)))

def agent(obs, config, model=model):

    def get_valid_actions(*, board, columns):
        return [c for c in range(columns) if board[c] == 0]

    model.eval()
    model = model.to(device)

    state = (obs['board'], obs['mark'])
    valid = get_valid_actions(board=obs['board'], columns=config['columns'])
    features = torch.tensor([encode_state_one_hot(state,
                                                  row_cnt=config['rows'],
                                                  col_cnt=config['columns'])
                            ], dtype=torch.float)
    with torch.no_grad():
        action = valid[model(features).squeeze()[valid].argmax().item()]
    return action
