from random import choice
import os
import torch
import torch.nn as nn
import torch.nn.functional as F


ROWS = 6
COLS = 7
ROOT = '/kaggle_simulations/agent'
WEIGHTS_PATH = '{}/target_net_6_7_4.pth'.format(ROOT)


class DQN(nn.Module):
    def __init__(self, n_observations, n_actions):
        super(DQN, self).__init__()
        self.layer1 = nn.Linear(n_observations, 128)
        self.layer2 = nn.Linear(128, 128)
        self.layer3 = nn.Linear(128, 128)
        self.layer4 = nn.Linear(128, 128)
        self.layer5 = nn.Linear(128, 128)
        self.layer6 = nn.Linear(128, n_actions)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        x = F.relu(self.layer3(x))
        x = F.relu(self.layer4(x))
        x = F.relu(self.layer5(x))
        return self.layer6(x)

agent_net = DQN(ROWS*COLS+1, COLS).to('cpu')
agent_net.load_state_dict(
    torch.load(WEIGHTS_PATH, map_location=torch.device('cpu')))


def state_from_obs(obs):
    return [*obs['board'], obs['mark']]

def get_valid_actions(states, n_actions=COLS, EMPTY=0):
    return [col for col in range(n_actions) if states[:, col] == EMPTY]

def agent(obs, config):
    state = state_from_obs(obs)
    state = torch.tensor(state, dtype=torch.float32, device='cpu').unsqueeze(0)
    valid = get_valid_actions(state)
    with torch.no_grad():
        action = valid[agent_net(state).squeeze()[valid].argmax().item()]
    return action
