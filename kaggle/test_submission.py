from kaggle_environments import evaluate
import numpy as np

agent = 'main.py'

agent_duels = [
    ['negamax', agent],
    [agent, 'negamax'],
    ['random', agent],
    [agent, 'random'],
]

for agents in agent_duels:
    rewards = evaluate('connectx', configuration=None, agents=agents, num_episodes=10)
    print('Agents:', agents)
    print('Wins:', np.mean(np.array(rewards) == 1, axis=0))
    print('(games: ', rewards, ')')
