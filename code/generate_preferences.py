import random

import tqdm

from tools import parse_arguments
from tools import get_config
from tools import get_states
from tools import get_board_and_mark
from tools import save_preferences

from negamax import negamax_agent
from negamax import Observation

if __name__ == '__main__':
    random.seed(0)
    arguments = parse_arguments()
    config = get_config(arguments)
    states = get_states(arguments)
    random.shuffle(states)

    features = []
    targets = []

    for idx in tqdm.tqdm(range(min(arguments['max_iteration'], len(states)))):
        state = states[idx]
        board, mark = get_board_and_mark(state)
        obs = Observation(board, mark)
        best, preferences = negamax_agent(obs, config, max_depth=4)
        features.append(state)
        targets.append(tuple(preferences))

    save_preferences(arguments=arguments, features=features, targets=targets)
