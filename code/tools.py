import argparse
from pathlib import Path
import json

from kaggle_environments import make
from kaggle_environments import evaluate
from kaggle_environments.utils import structify
from kaggle_environments.envs.connectx.connectx import EMPTY
from kaggle_environments.envs.connectx.connectx import play
from kaggle_environments.envs.connectx.connectx import is_win
from kaggle_environments.envs.connectx.connectx import negamax_agent


State = tuple[str, int] # (board, mark) where board is string from C-like array

def get_valid_actions(*, board: list[int], columns: int) -> list[int]:
    return [c for c in range(columns) if board[c] == EMPTY]

def is_full(*, board: list[int], config, has_played: bool=True) -> bool:
    moves = sum(1 if cell != EMPTY else 0 for cell in board)
    if not has_played:
        moves += 1
    size = config.rows * config.columns
    return moves == size

def get_board_and_mark(state: State) -> tuple[list[int], int]:
    board_str, mark = state
    board = [int(s) for s in board_str]
    return board, mark

def get_opponent_mark(mark: int) -> int:
    # Smart way of doing:((mark-offset+shift)%count)+offset
    return mark%2+1

def get_state(*, board: list[int], mark: int) -> State:
    return (''.join(str(i) for i in board), mark)

def parse_arguments() -> dict[str, int]:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-m', '--max_iteration', type=int,
                        default=10**6, help='maximum number of iterations')
    parser.add_argument('-r', '--rows', type=int,
                        default=3, help='number of rows')
    parser.add_argument('-c', '--columns', type=int,
                        default=3, help='number of columns')
    parser.add_argument('-i', '--inarow', type=int,
                        default=3, help='number of marks required in-a-row')
    return vars(parser.parse_args())

def get_config(arguments: dict) -> dict:
    return structify({'columns': arguments['columns'],
                      'rows': arguments['rows'],
                      'inarow': arguments['inarow']})

def get_path(*, name: str, arguments: dict):
    return (Path(__file__).parent.parent
            / 'data'
            / (name + '_r_{rows}_c_{columns}_i_{inarow}_m_{max_iteration}.json'
               .format(**arguments)))

def get_states(arguments) -> list[State]:
    path = get_path(name='states', arguments=arguments)

    with path.open('rt') as json_file:
        collection = json.load(json_file)
    return [tuple(state) for state in collection['states']]

def save_features_and_targets(*, name, arguments, features, targets):
    collection = dict(arguments=arguments, features=features, targets=targets)
    path = get_path(name=name, arguments=arguments)
    with path.open('wt') as json_file:
        json.dump(collection, json_file)

def save_transitions(*, arguments, features, targets):
    save_features_and_targets(name='transitions',
                              arguments=arguments,
                              features=features,
                              targets=targets)

def save_preferences(*, arguments, features, targets):
    save_features_and_targets(name='preferences',
                              arguments=arguments,
                              features=features,
                              targets=targets)
