from random import choice
from collections import namedtuple

import numpy as np

from tools import make
from tools import evaluate
from tools import parse_arguments
from tools import get_config

Observation = namedtuple('Observation', ('board', 'mark'))
Configuration = namedtuple('Configuration', ('columns', 'rows', 'inarow'))


# DISCLAIMER: The code was rewritten from kaggle_environments source code:
# https://github.com/Kaggle/kaggle-environments/blob/master/kaggle_environments/envs/connectx/connectx.py
EMPTY = 0


def play(board, column, mark, config):
    columns = config.columns
    rows = config.rows
    row = max([r for r in range(rows) if board[column + (r * columns)] == EMPTY])
    board[column + (row * columns)] = mark


def is_win(board, column, mark, config, has_played=True):
    columns = config.columns
    rows = config.rows
    inarow = config.inarow - 1
    row = (
        min([r for r in range(rows) if board[column + (r * columns)] == mark])
        if has_played
        else max([r for r in range(rows) if board[column + (r * columns)] == EMPTY])
    )

    def count(offset_row, offset_column):
        for i in range(1, inarow + 1):
            r = row + offset_row * i
            c = column + offset_column * i
            if (
                r < 0
                or r >= rows
                or c < 0
                or c >= columns
                or board[c + (r * columns)] != mark
            ):
                return i - 1
        return inarow

    return (
        count(1, 0) >= inarow  # vertical.
        or (count(0, 1) + count(0, -1)) >= inarow  # horizontal.
        or (count(-1, -1) + count(1, 1)) >= inarow  # top left diagonal.
        or (count(-1, 1) + count(1, -1)) >= inarow  # top right diagonal.
    )


def negamax_agent(obs, config,
                  # Due to compute/time constraints the tree depth must be limited.
                  max_depth = 4) -> int:
    """Returns column to drop a piece (random if ...)."""
    columns = config.columns
    rows = config.rows
    size = rows * columns


    def negamax(board, mark, depth) -> tuple[float, list[float]]:
        """Returns:
            tuple(best_value, values)
            best_value = number of empty cells before winning move.
        """
        best_value = -size
        values = [best_value]*columns

        moves = sum(1 if cell != EMPTY else 0 for cell in board)
        moves_left = size - moves

        # Tie Game
        if moves_left == 0:
            return (moves_left, values)

        # Can win next.
        for column in range(columns):
            if board[column] == EMPTY and is_win(board, column, mark, config, False):
                values[column] = moves_left
                best_value = values[column]
        if best_value > 0:
            return (best_value, values)


        # Recursively check all columns.
        for column in range(columns):
            if board[column] == EMPTY:
                # Max depth reached. Score based on cell proximity for a clustering effect.
                if depth <= 0:
                    row = max(
                        [
                            r
                            for r in range(rows)
                            if board[column + (r * columns)] == EMPTY
                        ]
                    )
                    values[column] = 0 # Estimated value is draw.
                    bonus = 1/5 # Bonus for case there are marks in row.
                    if column > 0 and board[row * columns + column - 1] == mark:
                        values[column] += bonus
                    if (column > 0
                        and row > 0
                        and board[(row - 1) * columns + column - 1] == mark
                    ):
                        values[column] += bonus
                    if (
                        column < columns - 1
                        and board[row * columns + column + 1] == mark
                    ):
                        values[column] += bonus
                    if (
                        column < columns - 1
                        and row > 0
                        and board[(row - 1) * columns + column + 1] == mark
                    ):
                        values[column] += bonus
                    if row > 0 and board[(row - 1) * columns + column] == mark:
                        values[column] += bonus
                else:
                    next_board = board.copy()
                    play(next_board, column, mark, config)
                    (score, _) = negamax(next_board, mark%2+1, depth - 1)
                    values[column] = score * -1
                if values[column] > best_value:
                    best_value = values[column]

        return (best_value, values)

    best_score, values = negamax(obs.board[:], obs.mark, max_depth)
    return best_score, values

def agent_wrapper(obs, config):
    best, values = negamax_agent(obs, config)
    return choice([idx for idx, value in enumerate(values) if value == best])

if __name__ == '__main__':
    arguments = parse_arguments()
    states = [
        ([2, 1, 0,
          1, 2, 2,
          1, 2, 1,
         ], 1),
        ([1, 2, 0,
          2, 1, 2,
          1, 1, 2,
         ], 1),
        ([1, 2, 0,
          2, 1, 0,
          1, 1, 2,
         ], 2),
        ([0, 2, 0,
          2, 1, 0,
          1, 1, 2,
         ], 1),
        ([0, 0, 0,
          2, 1, 0,
          1, 1, 2,
         ], 2),
        ([0, 0, 0,
          2, 0, 0,
          1, 1, 2,
         ], 1),
        ([0, 0, 0,
          2, 0, 0,
          1, 1, 0,
         ], 2),
        ([0, 0, 0,
          2, 0, 0,
          1, 0, 0,
         ], 1),
        ([0, 0, 0,
          2, 0, 0,
          1, 0, 1,
         ], 2),
        ([0, 0, 0,
          2, 0, 0,
          1, 2, 1,
         ], 1),
        ([0, 0, 0,
          2, 1, 0,
          1, 2, 1,
         ], 2),
        ([0, 0, 0,
          0, 0, 0,
          1, 2, 0,
         ], 1),
        ([0, 0, 0,
          0, 1, 0,
          1, 2, 0,
         ], 2),
        ([0, 2, 0,
          0, 1, 0,
          1, 2, 0,
         ], 1),
        ([0, 2, 0,
          0, 1, 0,
          1, 2, 1,
         ], 2),
        ([0, 0, 0,
          0, 0, 0,
          1, 0, 0,
         ], 2),
    ]

    debug = False
    if debug:
        for board, mark in states:
            print(board, mark, 'empty=', sum(e == 0 for e in board))
            obs = Observation(board, mark)
            config = Configuration(3, 3, 3)
            # breakpoint()
            best, values = negamax_agent(obs, config, max_depth=7)
            print(f'{best=}, {values=}\n')

        print('wrapper', agent_wrapper(obs, config))#, max_depth=7))

    config = get_config(arguments)
    print(f'{config=}')

    # agents = ['negamax', 'negamax']
    # agents = [agent_wrapper, agent_wrapper]
    agents = [agent_wrapper, 'negamax']

    rewards = evaluate('connectx',
                       agents[::-1],
                       configuration=config,
                       num_episodes=10)
    print(agents[::-1])
    print(rewards)
    print(np.mean(np.array(rewards) == 1, axis=0))

    rewards = evaluate('connectx',
                       agents,
                       configuration=config,
                       num_episodes=10)
    print(agents)
    print(rewards)
    print(np.mean(np.array(rewards) == 1, axis=0))
