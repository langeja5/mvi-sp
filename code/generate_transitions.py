import random

import tqdm

from tools import parse_arguments
from tools import get_states
from tools import get_board_and_mark
from tools import get_valid_actions
from tools import get_state
from tools import play
from tools import get_config
from tools import get_opponent_mark
from tools import save_transitions



if __name__ == '__main__':
    arguments = parse_arguments()
    config = get_config(arguments)
    states = get_states(arguments)
    random.shuffle(states)

    features = []
    targets = []

    for idx in tqdm.tqdm(range(min(arguments['max_iteration'], len(states)))):
        state = states[idx]
        board, mark = get_board_and_mark(state)
        for column in get_valid_actions(board=board,
                                        columns=arguments['columns']):
            board_copy = board.copy()
            play(board=board_copy, column=column, mark=mark, config=config)

            features.append([state, column])
            targets.append(
                get_state(board=board_copy, mark=get_opponent_mark(mark)))

    save_transitions(arguments=arguments, features=features, targets=targets)
