import json
import logging

import tqdm

from tools import EMPTY
from tools import State
from tools import make
from tools import play
from tools import get_valid_actions
from tools import is_full
from tools import is_win
from tools import get_board_and_mark
from tools import get_state
from tools import parse_arguments
from tools import get_path
from tools import get_config
from tools import get_opponent_mark


logging.basicConfig(level=logging.INFO) # level=logging.DEBUG


def generate(all_states: set[State], unexpanded: set[State], max_iteration: int, config):
    """Adds expanded states from unexpanded, to all_states and to unexpanded."""
    for iteration in tqdm.tqdm(range(max_iteration)):
        if len(unexpanded) == 0:
            break

        state = unexpanded.pop()
        board, mark = get_board_and_mark(state)

        if is_full(board=board, config=config, has_played=False):
            continue # If there is one move left, continue.

        for column in get_valid_actions(board=board, columns=config['columns']):
            if is_win(board=board,
                      column=column,
                      mark=mark, config=config, has_played=False):
                continue # If this is a winning move, continue.
            board_copy = board.copy()
            play(board=board_copy, column=column, mark=mark, config=config)
            state = get_state(board=board_copy, mark=get_opponent_mark(mark))

            unexpanded.add(state)
            all_states.add(state)

    logging.info(f'{iteration=}, {len(all_states)=}, {len(unexpanded)=}')
    return all_states, unexpanded


if __name__ == '__main__':
    arguments = parse_arguments()
    print('\n'.join(f'{key}={value}' for key, value in arguments.items()))

    env = make('connectx', configuration=get_config(arguments), debug=True)
    config = env.configuration

    all_states = set()
    unexpanded = set()

    size = config.rows*config.columns
    board_and_mark = (str(EMPTY)*size, 1)
    all_states.add(board_and_mark)
    unexpanded.add(board_and_mark)
    generate(all_states=all_states,
             unexpanded=unexpanded,
             max_iteration=arguments['max_iteration'],
             config=config)

    valid_states = dict(
        max_iteration = arguments['max_iteration'],
        configuration = config,
        states = sorted(list(all_states), key=lambda state: state[0])
    )

    with get_path(name='states', arguments=arguments).open('wt') as json_file:
        json.dump(valid_states, json_file)
