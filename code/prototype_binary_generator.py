import random
import itertools
import sys

import numpy as np


def show_state(state):
    print(f"{state[0]:0>42b}\n{state[1]:0>42b}")

uint_max = 18_446_744_073_709_551_615

rows, cols = (int(i) for i in sys.argv[1:])
print(f'{rows, cols=}')
base = 2
state_size = rows*cols
print(f'{state_size=}')

state = '1'*state_size
print(f'{state=}')

state_int = int(state, base=base)
print(f'{state_int=}')

state_int_max = base**state_size -1
print(f'{state_int > uint_max=}')
print(f'{state_int_max=}')
print(f'     {uint_max=}')

all_state_ints = range(state_int_max+1)

# intersecting = a & b
# disproportionate = abs(bin(a).count('1') - bin(b).count('1')) > 1
states_arr = np.array([(a, b) # [a, b]
    for a, b in itertools.product(all_state_ints, repeat=2)
        if not((a & b) or (abs(bin(a).count('1') - bin(b).count('1')) > 1))
], dtype=np.uint64)
print(f'{states_arr.shape=}')

for i in range(10):
    idx = random.randrange(0, states_arr.shape[0])
    print(f'\n{idx=}')
    show_state(states_arr[idx])

# >>> s = 'abcdefg'*6
# >>> s
# 'abcdefgabcdefgabcdefgabcdefgabcdefgabcdefg'
# >>> s[::7]
# 'aaaaaa'
# >>> [s[i::7] for i in range(6)]
# ['aaaaaa', 'bbbbbb', 'cccccc', 'dddddd', 'eeeeee', 'ffffff']
# >>> [s[i::7] for i in range(7)]
# ['aaaaaa', 'bbbbbb', 'cccccc', 'dddddd', 'eeeeee', 'ffffff', 'gggggg']
# .find returns lowest index -7 for last

# np.zeros(states, dtype=int)
# boards = list(itertools.product(options, repeat=rows*cols))
