\documentclass[a4paper,10pt,twocolumn]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage{booktabs}
\usepackage[top=0.5cm,bottom=2cm,left=1cm,right=1cm]{geometry}
\usepackage{hyperref}
\hypersetup{
     colorlinks=true, % Otherwise the links are in color boxes.
     linkcolor=black, % E.g. hyperlinks in table of contents.
     citecolor=black,
     urlcolor=blue, % Classical blue web links.
     }

\title{Connect X - Dipping a Toe into Reinforcement Learning \\
       \vspace{0.2cm}
       \normalsize{\href{https://gitlab.fit.cvut.cz/langeja5/mvi-sp}{gitlab.fit.cvut.cz/langeja5/mvi-sp}} \\
       \vspace{0.05cm}
       \normalsize{\href{https://www.kaggle.com/code/jaroslavlanger/connect-x-dqn}{kaggle.com/code/jaroslavlanger/connect-x-dqn}}
}
\date{\today}
\author{Jaroslav Langer / langeja5@fit.cvut.cz / FIT, CTU, Prague}
\begin{document}

\maketitle

\section{Introduction}

\href{https://www.kaggle.com/competitions/connectx}{Connect X} is a tic-tac-toe like game created by Kaggle.
It is an extension of a famous board game \href{https://en.wikipedia.org/wiki/Connect_Four}{Connect Four} published in the 70s.
The game is very similar to tic-tac-toe with one key difference.
In Connect X you can not put your \verb^mark^ on any empty spot.
Instead you choose a column in which the mark falls top-to-bottom the last empty spot.
Winning is identical to tic-tac-toe.
You win, if you have $i$ marks "in-a-row" either horizontally, vertically or diagonally.
Default Connect X setting is the same as Connect Four.
That is $6$ rows by $7$ columns with $i$ set to $4$.

\begin{figure}[H]
      \begin{center}
            \includegraphics[width=6cm]{../figures/walter_s_image.png}
      \end{center}
      \caption{Example of \href{https://storage.googleapis.com/kaggle-media/competitions/ConnectX/Walter's\%20image.png}{Connect X}.}
\end{figure}

\section{Data}

This Kaggle competition has no prepared dataset.
Instead they provided an learning \href{https://github.com/Kaggle/kaggle-environments/blob/master/README.md}{environment}.
It provides the simulation and evaluation capabilities.
Besides that, it has two prepared agents \verb^random^ and \verb^negamax^ out of the box.
These conditions lead to two obvious approaches to get some data to learn from.
First is to pre-generate the data by simulation.
And the second is to let the agent learn by playing i.e. generate the data continuously.

\subsection{Valid States}

Let's call a \verb^state^ some embedding of \verb^board^ and \verb^mark^.
The simplest may be just concatenation of the numbers. E.g.

\begin{verbatim}
#  Example of 3x3 state
board = [2,0,0, 1,0,0, 1,2,0]
mark = 1
state(board, mark) == "2001001201"
\end{verbatim}

For given number of rows $r$, columns $c$ and number of required marks in-a-row $i$.
I generated datasets of valid states \verb^states_c_{}_r_{}_i_{}_m_{}.csv^.
Because the number of states grows exponentially, I added argument $m$ which stands for maximum iteration (not maximum number of states).

\begin{verbatim}
# Example of states_r_5_c_4_i_3_m_1000000.csv
{
    "arguments": {"rows": 5, "columns": 4, "inarow": 3},
    "states": [state_a, state_b, ...]
}
\end{verbatim}

I generated it using two sets.
Set of \verb^all_states^ and set of \verb^expandable^.
In every iteration I popped a state from expandable and took all the valid actions.
For every created state, if it was expandable I added it both to \verb^all_states^ and to \verb^expandable^.

As there is a lot of possible configurations I arbitrarily chose these four. In all cases I set max-iteration to one million.

\begin{figure}[H]
    \begin{center}
    \begin{tabular}{ccc}
    \toprule
    Rows & Columns & In-a-row \\
    \midrule
    3 & 3 & 3 \\
    4 & 4 & 3 \\
    5 & 5 & 4 \\
    6 & 7 & 4 \\
    \bottomrule
    \end{tabular}
    \end{center}
    \caption{Game configurations explored.}
\end{figure}


\underline{Note}: For small boards this dataset could be generated exhaustively.
However there is no point in doing that, as the goal is to get to board sizes where this is not useful.
Partially because even if you have all the possible states generated you can not use it anyhow as their size is just too big.

\underline{Note}:
Good question is what dataset size is adequate.
The MNIST training dataset is $60,000\times28\times28$ (samples x pixels) and there are $10$ targets.
In the simplest approximation there is $\frac{60,000\times28\times28}{10}$ data points to learn one target of this complexity.
Something similarly big should be sufficient.
Although I assume recognizing patterns in Connect X to be a little bit harder.

\subsection{Valid Transitions}

Using the generated valid states I created dataset of state transitions.
Where features are a combination of a \verb^state^ and \verb^action^.
Target is the \verb^state^ created by applying an \verb^action^ onto the previous \verb^state^.

\begin{verbatim}
# transitions_r_5_c_4_i_3_m_1000000.csv
{
    "arguments": {"rows": 5, "columns": 4, "inrow": 3},
    "features": [state_and_action_a, ...],
    "targets": [state_a, ...]
}
\end{verbatim}

\subsection{Action Preferences}

Kaggle provides a negamax agent.
Negamax is an alternative variant of MiniMax algorithm.
Where during the return from recursion it negates the value sign.
So it always maximizes the negation of opponents value i.e. Nega-Max.
(In contrast to minimizing opponent's and maximizing one's own value Mini-Max.)
Kaggle's implementation returns the action to take.
I reimplemented it to return action preference instead.
The preference $p$ belong to one of five distinct ranges:
$p \in [-\textrm{size}, -1]$: opponent wins with $p-1$ empty cells left on the board.
$p \in (-1, 0)$: nothing is certain, but opponent will have some marks in-a-row.
$p = 0$: either a draw or position with no obvious advantage.
$p \in (0, 1)$: this leads to having some marks in-a-row.
$p \in [1, \textrm{size})$: you win with $p-1$ empty cells left.
This implementation solves a lot of troubles.
Perhaps for an invalid move $p=-\textrm{size}$ which is equivalent to lose in first move (e.g. it can not be worse).
Obviously it solves the problem of actions with the same preferences.
You don't need to choose between them.

Using this implementation I generated \verb^preferences_r_{}_c_{}_i_{}_m_{}.csv^.
Where for every state (features) is corresponding target - negamax action preferences.

\section{Deep Learning}

\subsection{Models}

I tried out two neural network model architectures.

First model has all layers fully connected and ReLU activation function is applied between them.
Number of neurons in hidden layers and the number of hidden layers itself are configurable parameters.

Second model is an extension of the first, where one convolutional layer is added before the sequential part.
Only the "board" part of the state is two-dimensional.
That is why this model must know the "convolutional shape".
In the forward function it splits the input to "conv" section and "flat" section, the convolution is done only to the "conv" section.
The output from convolutional layer is concatenated with the "flat" section and this is the input to the sequential part of the network.

For simplicity I will call the first model "Sequential" and the second "Convolutional".

\subsection{Learn Game Dynamics}

The task is to model $\textrm{transition}: \textrm{(state, action)} \to \textrm{state}$.

For both features (state, action) and target (state) I tried out two different encodings.
First the simple concatenation and second the one-hot encoding.
The encoding of the target leads to different loss and accuracy functions.

\begin{figure}[H]
    \begin{center}
    \begin{tabular}{lrr}
    \toprule
    Encoding \textbackslash Function & Loss &   Accuracy  \\
    \midrule
    Concatenation & MSE &  Accuracy(Round($x$)) \\
    One-Hot & BCE &  Accuracy($x > 0$)  \\
    \bottomrule
    \end{tabular}
    \end{center}
    \caption{Loss and accuracy functions for different target encodings.}
\end{figure}

I tried three combinations of features-target encodings.

\begin{figure}[H]
    \begin{center}
    \begin{tabular}{ll}
    \toprule
    Features & Target \\
    \midrule
    concatenation & concatenation \\
    one-hot & concatenation \\
    one-hot & one-hot \\
    \bottomrule
    \end{tabular}
    \end{center}
    \caption{Explored combinations of features-target encodings.}
\end{figure}

\subsection{Move Preferences}

Here the model should learn $\textrm{state} \to \textrm{preferences}$.
State encoding are the same from \textbf{Transitions}.
Preferences are taken "as is" a vector of numbers.

For measuring the accuracy of move preferences I created my own accuracy function.
Let's call it "preference accuracy".
It takes all ArgMax values of the model's output and checks if all of them are in the ArgMax values of the target.
So if the output has only one maximum (which will be true most of the time I think),
it checks whether the index is in the indices of maximum values in the target.
Because the game is symmetrical the target has very often multiple maximum values.

\subsection{Deep Value Network (DQN)}

Knowing a model with 4 hidden layers of 128 neurons can approximate negamax reasonably well.
I tried to learn it from scratch via self-play.
I used \href{https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html}{Pytorch's Reinforcement learning tutorial} as a starting point and changed it to learn Connect X.
As opponents for the self play I used Random agent, Negamax and the DQN learned in previous round.
Idea of having three opponents instead of only one is to ensure the DQN agent will be robust.
Without the random agent it would be possible a lot of states to be under visited.
I.e. its values might be under-trained.
The Negamax serves as a stable baseline who expects the other player to play a perfect game (of given depth).
I think it would be possible without the Negamax that the DQN would overfit his own weaknesses i.e. it might oscillate much more.

Previously to DQN I also tried to learn classical Q-table for a board $3 \times 3$.
It was successful but it showed me, how sensitive the Q-learning is to parameters setting such as $epsilon$, $gamma$ etc.

\begin{figure}[H]
      \begin{center}
            \includegraphics[width=8cm]{../figures/q_learning.png}
      \end{center}
      \caption{Learning of Q-table for a board $3 \times 3$.}
\end{figure}

\section{Results}

\subsection{Transition Network}

For every generated dataset and every encoding combination I reached 99.9\% accuracy with both models.
However, using the one-hot encoding for both features and target speeded up the learning process a lot.
For comparison in one setting the concat--concat took 30 minutes, one-hot--concat took 14 minutes and the one-hot--one-hot took only one minute and something.

\begin{figure}[H]
      \begin{center}
            \includegraphics[width=8cm]{../figures/learning_conv.png}
      \end{center}
      \caption{Example of Convolutional model learning transitions.}
      \label{conv}
\end{figure}

\underline{Note}: The Convolutional model's learning curve was often more step-like in comparison to the Sequential model. You can see it in Figure \ref{conv}.

\subsection{Negamax Preferences}

The bigger the board is the worse accuracies I obtained. For the board $6 \times 7$ I got to the test accuracy of $.87$.
Whereas for a board $4 \times 4$ I got to over $.95$.
These numbers can be interpreted in a way that the neural agent would take the same action as negamax in 87\%, 95\% cases respectively.
Interestingly the only board where was a big gap between train and test loss/accuracy was the $3 \times 3$ board.
All other cases didn't suffer from overhitting at all.
They all ended up in a long stable plateau.

For a board $6 \times 7$ I created an agent who plays valid action (separate check) with the highest preference.
Then I let it play against negamax.
The results where about 9:1 when negamax starts and 3:7 when negamax plays second.

\begin{figure}[H]
      \begin{center}
            \includegraphics[width=8cm]{../figures/linear_6_7_one_hot.png}
      \end{center}
      \caption{Example of learning Negamax's preferences.}
\end{figure}

\subsection{Deep Value Network}

Having the prior information from Q-learning and having the preset values from the Pytorch tutorial.
I made 3 big learning rounds where each was run for couple of hours.
At the end I saved the trained weights for the next round.
The result DQN agent plays just a little better than a random agent.
It is not capable of beating a negamax.

\begin{figure}[H]
      \begin{center}
            \includegraphics[width=8cm]{../figures/dqn_6_7_4.png}
      \end{center}
      \caption{Deep Value Network learn by self-play. Possible rewards are -1, 0, 1, with stabilizing $epsilon$, also the performance stabilize. The mean is calculated of all play variants i.e. first/second against all three opponents.}
\end{figure}

\section{Conclusions}

I trained several models to play a tic-tac-toe like game called Connect X.
The game environment was provided by Kaggle as it is an ongoing competition there for past two years.
I tried both linear and convolutional input layers.
I also tried two encodings "concat" and "one-hot".

For \textbf{valid transitions} all models with all encodings learned perfect transitions.
In terms of \textbf{negamax preferences} the model learns to approximate negamax, so they chose the same action in the range of $.7$ up to $.95$ cases depending on the game and model sizes.

Reinforcement learning of \textbf{deep value network} was not very successful. I was not able to learn the agent to play better than negamax nor the approximated negamax.
For comparison at the time of this writing on the Kaggle's leader board the DQN agent had ELO of 123, while the approximated negamax had 400.

In conclusion, using simple neural network to "compress" pre-computed values by Negamax works nicely.
This approach combines advantages from both worlds.
Negamax provides the optimal moves and Neural Network finds clever embedding so you don't have to to either calculate it every time or remember all the precomputed states.
(Because you train one agent to play against all opponents the unlimited negamax is the optimal policy.)

In terms of Reinforcement Learning it's much harder to get it right.
The setting of $\alpha$, its decay, choice of $\epsilon$ and its decay, and the choice of $\gamma$ makes it quite difficult.
Especially as the optimal values really depends on the size of the problem.
So values you find working for $4 \times 4$ board doesn't work well for $5 \times 5$ etc.

There are two directions worth following.
First, to pre-compute states with deeper negamax.
In this work I used depth 4 which is the default setting.
However the model I used doesn't care how laborious was the computing process.
So I can imagine that even $.8$ accuracy of negamax-40 could beat the negamax-4.
Second, option would be to explore deeper the seas of reinforcement learning.
In one discussion the current Kaggle's winner contemplate about his reinforcement learning agent's performance over different board sizes.
So the winning strategy currently is the Reinforcement learning, but it must be done right, which is hard.

\end{document}
