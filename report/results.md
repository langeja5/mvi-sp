Results
=======

States
------

**Encoding Concatenation**

```
Epoch 558 | 3.91e+03 backprops, mean values (last 10): {'loss train': 0.004, 'loss test': 0.006, 'acc train': 0.999, 'acc test': 0.995}
{'batch_size': 128,
 'data': 'transitions_r_3_c_3_i_3_m_1000000.json',
 'fn_acc': 'AccuracyRoundEqualAllMean',
 'fn_loss': 'MSELoss()',
 'model': 'TransitionNet(\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=11, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=10, bias=True)\n'
          '  )\n'
          ')',
 'shape_test_feat': (194, 11),
 'shape_test_targ': (194, 10),
 'shape_train_feat': (772, 11),
 'shape_train_targ': (772, 10)}
CPU times: user 27.2 s, sys: 620 ms, total: 27.9 s
Wall time: 28.2 s
```

```
Epoch 38 | 1.56e+04 backprops, mean values (last 10): {'loss train': 0.003, 'loss test': 0.003, 'acc train': 0.995, 'acc test': 0.995}
{'batch_size': 128,
 'data': 'transitions_r_4_c_4_i_3_m_1000000.json',
 'fn_acc': 'AccuracyRoundEqualAllMean',
 'fn_loss': 'MSELoss()',
 'model': 'TransitionNet(\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=18, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=17, bias=True)\n'
          '  )\n'
          ')',
 'shape_test_feat': (13152, 18),
 'shape_test_targ': (13152, 17),
 'shape_train_feat': (52604, 18),
 'shape_train_targ': (52604, 17)}
CPU times: user 49.7 s, sys: 861 ms, total: 50.6 s
Wall time: 50.9 s
```

```
Epoch 23 mean of last 10: {'loss train': 0.001, 'loss test': 0.001, 'acc train': 0.995, 'acc test': 0.997}
{'batch_size': 128,
 'data': 'transitions_r_5_c_5_i_4_m_1000000.json',
 'fn_acc': 'AccuracyRoundEqualAllMean',
 'fn_loss': 'MSELoss()',
 'model': 'TransitionNet(\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=27, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=26, bias=True)\n'
          '  )\n'
          ')',
 'shape_test_feat': (796647, 27),
 'shape_test_targ': (796647, 26),
 'shape_train_feat': (3186586, 27),
 'shape_train_targ': (3186586, 26)}
CPU times: user 30min 58s, sys: 32.6 s, total: 31min 31s
Wall time: 31min 37s
```

```
Epoch 59 | 2.51e+06 backprops, mean values (last 10): {'loss train': 0.012, 'loss test': 0.013, 'acc train': 0.738, 'acc test': 0.738}
Epoch 60 | 2.56e+06 backprops, mean test accuracy (last 10): 0.7418696917999746
Epoch 61 | 2.60e+06 backprops, mean test accuracy (last 10): 0.7444534924638937
Epoch 62 | 2.64e+06 backprops, mean test accuracy (last 10): 0.7462167662026904
Epoch 63 | 2.68e+06 backprops, mean test accuracy (last 10): 0.7471188463760843
Est. 2h15m
```

**One-Hot Encoding Features**

```
Epoch 7 | 1.99e+05 backprops, mean values (last 10): {'loss train': 0.0048, 'loss test': 0.0025, 'acc train': 0.9709, 'acc test': 0.9956}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'linear',
 'model_str': 'TransitionNet(\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=82, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=26, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'transitions'}
CPU times: user 11min 10s, sys: 2.49 s, total: 11min 13s
Wall time: 11min 18s
```

**One-Hot Encoding Features and Targets**

```
Epoch 281 | 1.97e+03 backprops, mean values (last 10): {'loss train': 0.003, 'loss test': 0.004, 'acc train': 1.0, 'acc test': 0.995}
{'batch_size': 128,
 'data': 'transitions_r_3_c_3_i_3_m_1000000.json',
 'fn_acc': 'AllEqualMeanOfGreaterThanZero',
 'fn_loss': 'BCEWithLogitsLoss()',
 'model': 'TransitionNet(\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=32, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=29, bias=True)\n'
          '  )\n'
          ')',
 'shape_test_feat': (194, 32),
 'shape_test_targ': (194, 29),
 'shape_train_feat': (772, 32),
 'shape_train_targ': (772, 29)}
CPU times: user 6.52 s, sys: 132 ms, total: 6.65 s
Wall time: 6.65 s
```

```
Epoch 13 | 5.75e+03 backprops, mean values (last 10): {'loss train': 0.002, 'loss test': 0.001, 'acc train': 0.997, 'acc test': 0.999}
{'batch_size': 128,
 'data': 'transitions_r_4_c_4_i_3_m_1000000.json',
 'fn_acc': 'AllEqualMeanOfGreaterThanZero',
 'fn_loss': 'BCEWithLogitsLoss()',
 'model': 'TransitionNet(\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=54, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=50, bias=True)\n'
          '  )\n'
          ')',
 'shape_test_feat': (13152, 54),
 'shape_test_targ': (13152, 50),
 'shape_train_feat': (52604, 54),
 'shape_train_targ': (52604, 50)}
CPU times: user 19.9 s, sys: 283 ms, total: 20.1 s
Wall time: 20.3 s
```

```
Epoch 0 | 2.49e+04 backprops, mean values (last 10): {'loss train': 0.008, 'loss test': 0.0, 'acc train': 0.943, 'acc test': 1.0}
{'batch_size': 128,
 'data': 'transitions_r_5_c_5_i_4_m_1000000.json',
 'fn_acc': 'AllEqualMeanOfGreaterThanZero',
 'fn_loss': 'BCEWithLogitsLoss()',
 'model': 'TransitionNet(\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=82, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=77, bias=True)\n'
          '  )\n'
          ')',
 'shape_test_feat': (796647, 82),
 'shape_test_targ': (796647, 77),
 'shape_train_feat': (3186586, 82),
 'shape_train_targ': (3186586, 77)}
CPU times: user 1min 9s, sys: 1.25 s, total: 1min 10s
Wall time: 1min 9s
```

```
Epoch 12 | 5.54e+05 backprops, mean values (last 10): {'loss train': 0.0, 'loss test': 0.0, 'acc train': 0.994, 'acc test': 0.995}
{'batch_size': 128,
 'data': 'transitions_r_6_c_7_i_4_m_1000000.json',
 'fn_acc': 'AllEqualMeanOfGreaterThanZero',
 'fn_loss': 'BCEWithLogitsLoss()',
 'model': 'TransitionNet(\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=135, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=128, bias=True)\n'
          '  )\n'
          ')',
 'shape_test_feat': (1363755, 135),
 'shape_test_targ': (1363755, 128),
 'shape_train_feat': (5455018, 135),
 'shape_train_targ': (5455018, 128)}
CPU times: user 28min 32s, sys: 23.4 s, total: 28min 55s
Wall time: 28min 38s
```

**Convolution**

In contrast with linear architecture, the loss function decreased in steps.
I.e. the most of the progress was made in few iterations, and the others were plateau.

```
Epoch 666 | 4.67e+03 backprops, mean values (last 10): {'loss train': 0.0042, 'loss test': 0.0053, 'acc train': 0.992, 'acc test': 0.9953}
{'data': 'transitions_r_3_c_3_i_3_m_1000000.json',
 'model': 'TransitionConvNet(\n'
          '  (convolutional): Conv2d(1, 3, kernel_size=(4, 4), stride=(1, 1), '
          'padding=(3, 3))\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=110, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=10, bias=True)\n'
          '  )\n'
          ')'}
CPU times: user 21.3 s, sys: 430 ms, total: 21.8 s
Wall time: 23.9 s
```

```
Epoch 94 | 3.90e+04 backprops, mean values (last 10): {'loss train': 0.0016, 'loss test': 0.0015, 'acc train': 0.9945, 'acc test': 0.9961}
{'data': 'transitions_r_4_c_4_i_3_m_1000000.json',
 'model': 'TransitionConvNet(\n'
          '  (convolutional): Conv2d(1, 3, kernel_size=(4, 4), stride=(1, 1), '
          'padding=(3, 3))\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=149, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=17, bias=True)\n'
          '  )\n'
          ')'}
CPU times: user 2min 42s, sys: 3.09 s, total: 2min 45s
Wall time: 2min 48s
```

```
Epoch 36 | 9.21e+05 backprops, mean values (last 10): {'loss train': 0.0004, 'loss test': 0.0005, 'acc train': 0.9951, 'acc test': 0.9963}
{'data': 'transitions_r_5_c_5_i_4_m_1000000.json',
 'model': 'TransitionConvNet(\n'
          '  (convolutional): Conv2d(1, 3, kernel_size=(4, 4), stride=(1, 1), '
          'padding=(3, 3))\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=194, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=26, bias=True)\n'
          '  )\n'
          ')'}
CPU times: user 1h 27s, sys: 1min 10s, total: 1h 1min 38s
Wall time: 1h 1min 45s
```

**One-Hot Convolution**

```
Epoch 235 | 1.65e+03 backprops, mean values (last 10): {'loss train': 0.0018, 'loss test': 0.0027, 'acc train': 1.0, 'acc test': 0.9953}
{'data': 'transitions_r_3_c_3_i_3_m_1000000.json',
 'model': 'TransitionConvNet(\n'
          '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, 1), '
          'padding=(3, 3))\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=113, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=29, bias=True)\n'
          '  )\n'
          ')'}
CPU times: user 7.84 s, sys: 139 ms, total: 7.98 s
Wall time: 8.61 s
```

```
Epoch 11 | 4.93e+03 backprops, mean values (last 10): {'loss train': 0.0024, 'loss test': 0.0015, 'acc train': 0.9857, 'acc test': 0.9971}
{'data': 'transitions_r_4_c_4_i_3_m_1000000.json',
 'model': 'TransitionConvNet(\n'
          '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, 1), '
          'padding=(3, 3))\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=153, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=50, bias=True)\n'
          '  )\n'
          ')'}
CPU times: user 20.4 s, sys: 314 ms, total: 20.7 s
Wall time: 20.8 s
```

```
Epoch 11 | 2.99e+05 backprops, mean values (last 10): {'loss train': 0.0002, 'loss test': 0.0, 'acc train': 0.9962, 'acc test': 0.9991}
{'data': 'transitions_r_5_c_5_i_4_m_1000000.json',
 'model': 'TransitionConvNet(\n'
          '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, 1), '
          'padding=(3, 3))\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=199, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=77, bias=True)\n'
          '  )\n'
          ')'}
CPU times: user 20min 47s, sys: 18 s, total: 21min 5s
Wall time: 21min 8s
```

```
Epoch 20 | 8.95e+05 backprops, mean values (last 10): {'loss train': 0.0004, 'loss test': 0.0005, 'acc train': 0.9888, 'acc test': 0.9832}
{'data': 'transitions_r_6_c_7_i_4_m_1000000.json',
 'model': 'TransitionConvNet(\n'
          '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, 1), '
          'padding=(3, 3))\n'
          '  (sequential): Sequential(\n'
          '    (0): Linear(in_features=279, out_features=64, bias=True)\n'
          '    (1): ReLU()\n'
          '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
          '    (3): ReLU()\n'
          '    (4): Linear(in_features=64, out_features=128, bias=True)\n'
          '  )\n'
          ')'}
Wall time: 1h
```

Preferences
-----------

**Cosine Similarity**

```
Epoch 867 | 1.30e+05 backprops, mean values (last 10): {'loss train': 19.39, 'loss test': 21.3639, 'acc train': 0.923, 'acc test': 0.9185}
{'batch_size': 128,
 'config': {'columns': 4, 'inarow': 3, 'rows': 4},
 'debug': True,
 'encoding': 'concat',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'linear',
 'model_str': 'TransitionNet(\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=17, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=4, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
time 6m
```

```
Epoch 671 | 1.01e+05 backprops, mean values (last 10): {'loss train': 10.2124, 'loss test': 12.6444, 'acc train': 0.9536, 'acc test': 0.9457}
{'batch_size': 128,
 'config': {'columns': 4, 'inarow': 3, 'rows': 4},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'linear',
 'model_str': 'TransitionNet(\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=50, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=4, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
time 5m 45s
```

```
Epoch 692 | 1.04e+05 backprops, mean values (last 10): {'loss train': 9.9458, 'loss test': 12.3355, 'acc train': 0.9556, 'acc test': 0.9479}
{'batch_size': 128,
 'config': {'columns': 4, 'inarow': 3, 'rows': 4},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=149, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=4, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
time 8m
```

**Greater than zero accuracy**

```
Epoch 3884 | 5.83e+05 backprops, mean values (last 10): {'loss train': 7.3587, 'loss test': 9.8363, 'acc train': 0.8457, 'acc test': 0.822}
{'batch_size': 128,
 'config': {'columns': 4, 'inarow': 3, 'rows': 4},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'linear',
 'model_str': 'TransitionNet(\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=50, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=4, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
 time: 33m
```

**All ArgMax of Predictions are in ArgMax of Targets**

```
Epoch 678 | 1.02e+05 backprops, mean values (last 10): {'loss train': 10.4058, 'loss test': 13.0487, 'acc train': 0.9471, 'acc test': 0.9337}
{'batch_size': 128,
 'config': {'columns': 4, 'inarow': 3, 'rows': 4},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'linear',
 'model_str': 'TransitionNet(\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=50, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=4, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
time: 6m
```

```
Epoch 31 | 2.00e+05 backprops, mean values (last 10): {'loss train': 19.6433, 'loss test': 19.5482, 'acc train': 0.8206, 'acc test': 0.8187}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'linear',
 'model_str': 'TransitionNet(\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=77, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=5, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
time: 10m
```

```
Epoch 16 | 1.06e+05 backprops, mean values (last 10): {'loss train': 23.2778, 'loss test': 23.1846, 'acc train': 0.8062, 'acc test': 0.8058}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=194, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=5, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
CPU times: user 6min 35s, sys: 5.73 s, total: 6min 40s
Wall time: 6min 41s
```

```
Epoch 16 | 1.06e+05 backprops, mean values (last 10): {'loss train': 1.6509, 'loss test': 1.7743, 'acc train': 0.901, 'acc test': 0.9018}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 3, 'hidden_neurons': 128},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=194, out_features=128, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (5): ReLU()\n'
              '    (6): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (7): ReLU()\n'
              '    (8): Linear(in_features=128, out_features=5, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
CPU times: user 8min 18s, sys: 6.96 s, total: 8min 25s
Wall time: 8min 26s
```

```
Epoch 16 | 1.06e+05 backprops, mean values (last 10): {'loss train': 1.8141, 'loss test': 1.8229, 'acc train': 0.8961, 'acc test': 0.8953}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 6, 'hidden_neurons': 128},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=194, out_features=128, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (5): ReLU()\n'
              '    (6): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (7): ReLU()\n'
              '    (8): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (9): ReLU()\n'
              '    (10): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (11): ReLU()\n'
              '    (12): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (13): ReLU()\n'
              '    (14): Linear(in_features=128, out_features=5, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
CPU times: user 10min 18s, sys: 7.24 s, total: 10min 25s
Wall time: 10min 27s
```

```
Epoch 16 | 1.06e+05 backprops, mean values (last 10): {'loss train': 1.9264, 'loss test': 1.8477, 'acc train': 0.8979, 'acc test': 0.9006}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'conv_out': 6, 'hidden_cnt': 3, 'hidden_neurons': 128},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 6, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=386, out_features=128, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (5): ReLU()\n'
              '    (6): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (7): ReLU()\n'
              '    (8): Linear(in_features=128, out_features=5, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
CPU times: user 8min 19s, sys: 6.98 s, total: 8min 26s
Wall time: 8min 27s

```

```
Epoch 16 | 1.06e+05 backprops, mean values (last 10): {'loss train': 0.8895, 'loss test': 0.9443, 'acc train': 0.9131, 'acc test': 0.9117}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'conv_out': 6, 'hidden_cnt': 3, 'hidden_neurons': 256},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 6, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=386, out_features=256, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=256, out_features=256, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=256, out_features=256, bias=True)\n'
              '    (5): ReLU()\n'
              '    (6): Linear(in_features=256, out_features=256, bias=True)\n'
              '    (7): ReLU()\n'
              '    (8): Linear(in_features=256, out_features=5, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
CPU times: user 8min 18s, sys: 6.41 s, total: 8min 25s
Wall time: 8min 26s
```

```
Epoch 16 | 1.06e+05 backprops, mean values (last 10): {'loss train': 1.999, 'loss test': 1.9444, 'acc train': 0.8956, 'acc test': 0.8962}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'conv_out': 3, 'hidden_cnt': 4, 'hidden_neurons': 128},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=194, out_features=128, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (5): ReLU()\n'
              '    (6): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (7): ReLU()\n'
              '    (8): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (9): ReLU()\n'
              '    (10): Linear(in_features=128, out_features=5, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
CPU times: user 9min 17s, sys: 8.97 s, total: 9min 26s
Wall time: 9min 29s
```

```
Epoch 31 | 2.00e+05 backprops, mean values (last 10): {'loss train': 19.3152, 'loss test': 19.4246, 'acc train': 0.8104, 'acc test': 0.8077}
{'batch_size': 128,
 'config': {'columns': 5, 'inarow': 4, 'rows': 5},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=194, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=5, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
```

```
Epoch 168 | 1.06e+06 backprops, mean values (last 10): {'loss train': 94.1557, 'loss test': 95.0436, 'acc train': 0.7844, 'acc test': 0.7849}
{'batch_size': 128,
 'config': {'columns': 7, 'inarow': 4, 'rows': 6},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'hidden_cnt': 1, 'hidden_neurons': 64},
 'model_name': 'linear',
 'model_str': 'TransitionNet(\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=128, out_features=64, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=64, out_features=64, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=64, out_features=7, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
time: 56m
```

```
Epoch 16 | 1.06e+05 backprops, mean values (last 10): {'loss train': 24.768, 'loss test': 24.573, 'acc train': 0.8573, 'acc test': 0.8573}
{'batch_size': 128,
 'config': {'columns': 7, 'inarow': 4, 'rows': 6},
 'debug': True,
 'encoding': 'one-hot',
 'learning_rate': 0.001,
 'model_kwargs': {'conv_out': 3, 'hidden_cnt': 4, 'hidden_neurons': 128},
 'model_name': 'convolutional',
 'model_str': 'TransitionConvNet(\n'
              '  (convolutional): Conv2d(3, 3, kernel_size=(4, 4), stride=(1, '
              '1), padding=(3, 3))\n'
              '  (sequential): Sequential(\n'
              '    (0): Linear(in_features=272, out_features=128, bias=True)\n'
              '    (1): ReLU()\n'
              '    (2): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (3): ReLU()\n'
              '    (4): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (5): ReLU()\n'
              '    (6): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (7): ReLU()\n'
              '    (8): Linear(in_features=128, out_features=128, bias=True)\n'
              '    (9): ReLU()\n'
              '    (10): Linear(in_features=128, out_features=7, bias=True)\n'
              '  )\n'
              ')',
 'optimizer_class': <class 'torch.optim.adam.Adam'>,
 'task': 'preferences'}
```
